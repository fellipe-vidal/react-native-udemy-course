import React from 'react'
import { Platform, TouchableOpacity, StyleSheet } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'

import Colors from'../constants/colors'

const CustomHeaderButton = () => {
  return (
    <TouchableOpacity style={styles.constainer} onPress={() => console.log("favorited")} >
      <Ionicons name="ios-star" size={23} color={Platform.OS === "android" ? "#FFF" : Colors.primaryColor } />
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  constainer: {
    margin: 5,
    marginRight: 20
  }
})

export default CustomHeaderButton
