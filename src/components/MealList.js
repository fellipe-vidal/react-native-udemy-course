import React from 'react'
import { StyleSheet, View, FlatList } from 'react-native'

import MealItem from '../components/MealItem'

const MealList = ( {data, navigation} ) => {

  const renderMealItem = ({item}) => {
    return <MealItem 
            title={item.title} 
            duration={item.duration}
            complexity={item.complexity}
            affordability={item.affordability}
            image={item.imageUrl}
            onSelectMeal={() => {
              navigation.navigate("MealDetails", { mealId: item.id, mealTitle: item.title })
            }} />
  }

  return (
    <View style={styles.screen}>
      <FlatList data={data} renderItem={renderMealItem} style={{width: "100%"}}/>
    </View>
  )
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  }
})

export default MealList
