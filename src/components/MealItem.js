import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity, ImageBackground } from 'react-native'

const MealItem = ({title, duration, complexity, affordability, image, onSelectMeal}) => {
  return (
    <View style={styles.shadowBox}>
      <View style={styles.mealItem}>
        <TouchableOpacity onPress={onSelectMeal} >
          <View>

            <View style={{...styles.mealRow, ...styles.mealHeader}}>
              <ImageBackground source={{uri: image}} style={styles.bgImage} >
                <Text style={styles.title} numberOfLines={1} >{title}</Text>
              </ImageBackground>
            </View>

            <View style={{...styles.mealRow, ...styles.mealDetails}}>
              <Text>{duration}m</Text>
              <Text>{complexity.toUpperCase()}</Text>
              <Text>{affordability.toUpperCase()}</Text>
            </View>
            
          </View>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({

  mealItem : {
    height: 200,
    backgroundColor: "#F5F5F5",
    borderRadius: 10,
    overflow: "hidden"
    
  },

  shadowBox: {
    shadowColor: "#000",
    shadowOpacity: 0.15,
    shadowOffset: { height: 4, width: 0 },
    shadowRadius: 4,
    elevation: 3,
    marginHorizontal: 10,
    marginVertical: 10,
  },

  mealRow: {
    flexDirection: "row",
  },

  mealHeader: {
    height: "85%",
  },

  mealDetails: {
    height: "15%",
    paddingHorizontal: 20,
    justifyContent: "space-between",
    alignItems: "center"
  },

  bgImage: {
    height: "100%",
    width: "100%",
    justifyContent: "flex-end",

  },

  title: {
    fontWeight: "500",
    fontSize: 18,
    color: "#FFF",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    paddingVertical: 5,
    paddingHorizontal: 12,
    textAlign: "center",

  }

})

export default MealItem
