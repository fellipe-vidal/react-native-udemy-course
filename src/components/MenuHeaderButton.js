import React from 'react'
import { Platform, TouchableOpacity, StyleSheet } from 'react-native'
import Ionicons from 'react-native-vector-icons/Ionicons'

import Colors from'../constants/colors'

const MenuHeaderButton = ({navigation}) => {
  return (
    <TouchableOpacity style={styles.constainer} onPress={() => navigation.toggleDrawer()} >
      <Ionicons name="ios-menu" size={23} color={Platform.OS === "android" ? "#FFF" : Colors.primaryColor } />
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  constainer: {
    margin: 5,
    marginLeft: 20
  }
})

export default MenuHeaderButton