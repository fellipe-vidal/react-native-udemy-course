import React from 'react'
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native'

const CategoryGridTile = ({title, color, onSelect}) => {

  return (
    <TouchableOpacity style={styles.gridItem} onPress={ onSelect} >
      <View style={{...styles.container, ...{backgroundColor: color}}} >
        <Text style={styles.title}>{title} </Text>
      </View>
    </TouchableOpacity>
    
  );

}

const styles = StyleSheet.create({

  gridItem: {
    flex: 1,
    margin: 15,
    height: 150,
  },

  container: {
    flex: 1,
    borderRadius: 10,
    shadowColor: "#000",
    shadowOpacity: 0.35,
    shadowOffset: { height: 4, width: 0 },
    shadowRadius: 4,
    elevation: 3,
    padding: 15,
    justifyContent: "flex-end",
    alignItems: "flex-end",
  },

  title: {
    fontSize: 16,
    fontWeight: "500",
    textAlign: "right"
  }

})

export default CategoryGridTile;