import React, { useEffect } from 'react'
import { StyleSheet, View, Text, ScrollView, Image } from 'react-native'
import { useSelector } from 'react-redux'

import FavoriteHeaderButton from '../components/FavoriteHeaderButton'


const ListItem = ( props ) => {
  return(
    <View style={styles.listItem} >
      <Text>{props.children}</Text>
    </View>
  )
}

const MealDetailsScreen = ({navigation}) => {

  const meals = useSelector( ({meals}) => meals.meals )
  const mealId = navigation.getParam("mealId")
  const selectedMeal = meals.find( meal => meal.id === mealId )
  

  return (
    <ScrollView>
      <Image source={{uri: selectedMeal.imageUrl}} style={styles.image} />
      <View style={styles.details}>
        <Text>{selectedMeal.duration}m</Text>
        <Text>{selectedMeal.complexity.toUpperCase()}</Text>
        <Text>{selectedMeal.affordability.toUpperCase()}</Text>
      </View>
      <Text style={styles.title}>Ingredients</Text>
      { selectedMeal.ingredients.map( ingredient => (
        <ListItem key={ingredient} >{ingredient}</ListItem>
      ) ) }
      <Text style={styles.title}>Steps</Text>
      { selectedMeal.steps.map( step => (
        <ListItem key={step} >{step}</ListItem>
      ) ) }
    </ScrollView>
  )
}

MealDetailsScreen.navigationOptions = ({ navigation }) => {

  const mealTitle = navigation.getParam("mealTitle")


  return {
    headerTitle: mealTitle,
    headerRight: () => <FavoriteHeaderButton />

  }

}

const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: 200
  },

  details: {
    flexDirection: "row",
    padding: 15,
    justifyContent: "space-around",
  },

  title: {
    fontSize: 20,
    textAlign: "center",
    fontWeight: "bold",
  },

  listItem: {
    marginVertical: 10,
    marginHorizontal: 20,
    borderColor: "#CCC",
    borderWidth: 1,
    padding: 10,
  },
})

export default MealDetailsScreen