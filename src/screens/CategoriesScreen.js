import React from 'react'
import { StyleSheet, FlatList } from 'react-native'

import { categories } from '../data/dummy-data'

import CategoryGridTile from '../components/CategoryGridTile'
import MenuHeaderButton from '../components/MenuHeaderButton'


const CategoriesScreen = ({ navigation }) => {

  const renderGridItem = (item) => {
    return <CategoryGridTile title={item.title} color={item.color} onSelect={() => navigation.navigate("CategoryMeals", {categoryId: item.id})} /> 
  }

  return (
    <FlatList data={categories} renderItem={ ({item}) => renderGridItem(item)} numColumns={2}  />
  )
}

CategoriesScreen.navigationOptions = ({navigation}) => {

  return {
    headerLeft: () =>  <MenuHeaderButton navigation={navigation} /> 
  }

}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
})

export default CategoriesScreen
