import React from 'react'
import { useSelector } from 'react-redux'

import MealList from '../components/MealList'
import MenuHeaderButton from '../components/MenuHeaderButton'

const FavoritesScreen = ({navigation}) => {

  const favoriteMeals = useSelector( ({meals}) => meals.favoriteMeals )

  return <MealList data={favoriteMeals} navigation={navigation} />
}

FavoritesScreen.navigationOptions = ({navigation}) => {

  return {
    headerLeft: () =>  <MenuHeaderButton navigation={navigation} /> 
  }

}

export default FavoritesScreen
