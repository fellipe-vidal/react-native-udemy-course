import React, { useState } from 'react'
import { View, Text, StyleSheet, Switch, Platform, Button } from 'react-native'

import MenuHeaderButton from '../components/MenuHeaderButton'
import Colors from '../constants/colors'

const FilterSwitch = ({label, state, changeState}) => {
  return(
    <View style={styles.filterContainer} >
      <Text>{label}</Text>
      <Switch
        value={state}
        onValueChange={ value => changeState(value) }
        trackColor={{true: Colors.primaryColor}}
        thumbColor={Platform.OS === "android" ? Colors.primaryColor : "" } />
    </View>

  )
}

const FiltersScreen = () => {

  const [isGlutenFree, setGlutenFree] = useState(false)
  const [isLactoseFree, setLactoseFree] = useState(false)
  const [isVegan, setVegan] = useState(false)
  const [isVegetarian, setVegetarian] = useState(false)

  return (
    <View style={styles.screen}>
      <Text style={styles.title} >Avaible Filters / Restrictions</Text>
      <FilterSwitch
        label="Gluten-free"
        state={isGlutenFree}
        changeState={ value => setGlutenFree(value) }
      />
      <FilterSwitch
        label="Lactose-free"
        state={isLactoseFree}
        changeState={ value => setLactoseFree(value) }
      />
      <FilterSwitch
        label="Vegan"
        state={isVegan}
        changeState={ value => setVegan(value) }
      />
      <FilterSwitch
        label="Vegetarian"
        state={isVegetarian}
        changeState={ value => setVegetarian(value) }
      />
      <Button title="Save" color={Colors.primaryColor} ></Button>
    </View>
  )
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    alignItems: "center",
  },

  title: {
    fontWeight: "500",
    fontSize: 20,
    margin: 20,
    textAlign: "center",
  },

  filterContainer: {
    width: "90%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginVertical: 10,

  },
})

FiltersScreen.navigationOptions = ({navigation}) => {

  return {
    headerLeft: () =>  <MenuHeaderButton navigation={navigation} /> 
  }

}

export default FiltersScreen
