import React from 'react'
import { useSelector } from 'react-redux'

import { categories, meals } from '../data/dummy-data'

import MealList from '../components/MealList'

const CategoryMealsScreen = ({ navigation }) => {

  const availableMeals = useSelector( state => state.meals.filteredMeals )
  const displayedMeals = availableMeals.filter( meal => meal.categoryIds.indexOf(navigation.getParam("categoryId")) >= 0 ) 

  return <MealList data={displayedMeals} navigation={navigation} />
}

CategoryMealsScreen.navigationOptions = ( {navigation} ) => {

  const category = categories.find( item => item.id === navigation.getParam("categoryId") ) 

  return { headerTitle: category.title }

} 

export default CategoryMealsScreen
