import { meals } from '../../data/dummy-data'

const initialState = {
  meals,
  filteredMeals: meals,
  favoriteMeals: []
}

const mealsReducer = (state = initialState, action) => {
  return state;
}

export default mealsReducer;