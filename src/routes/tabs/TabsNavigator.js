import React from 'react'
import { createBottomTabNavigator } from 'react-navigation-tabs';

import Icon from 'react-native-vector-icons/Ionicons';
import Colors from '../../constants/colors';

import MealsStack from '../stacks/MealsStack'
import FavoritesStack from '../stacks/FavoritesStack'

const TabsNavigator = createBottomTabNavigator({
  Meals: {
    screen: MealsStack,
    navigationOptions: {
      tabBarIcon: (tabInfo) => <Icon name="ios-restaurant" size={25} color={tabInfo.tintColor} />,
    }
  },

  Favorites: {
    screen: FavoritesStack,
    navigationOptions: {
      tabBarIcon: (tabInfo) => <Icon name="ios-star" size={25} color={tabInfo.tintColor} /> 
    }
  }

}, {
  tabBarOptions: {
    activeTintColor: Colors.accentColor,
  }
})

export default TabsNavigator;