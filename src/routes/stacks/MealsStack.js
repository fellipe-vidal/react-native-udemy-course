import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';

import CategoriesScreen from '../../screens/CategoriesScreen';
import CategoryMealsScreen from '../../screens/CategoryMealsScreen';
import MealDetailsScreen from '../../screens/MealDetailsScreen';

import Colors from '../../constants/colors'

const MealsStack = createStackNavigator({
  AllCategories: {
    screen: CategoriesScreen,
    navigationOptions: {
      headerTitle: "Meal Categories",
    }
    
  },
  CategoryMeals: {
    screen: CategoryMealsScreen,

  },
  MealDetails: {
    screen: MealDetailsScreen,

  }
}, {
  defaultNavigationOptions: {
    headerTitle: "Meal Categories",
      headerStyle: Platform.select({
        android: { backgroundColor: Colors.primaryColor },

      }),
      headerTintColor: Platform.select({
        android: "#FFF",
        ios: Colors.primaryColor
        
      })
      ,
      
      headerBackTitle: "Back"
  }
})

export default MealsStack;
