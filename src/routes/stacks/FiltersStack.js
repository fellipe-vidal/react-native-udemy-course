import { createStackNavigator } from 'react-navigation-stack'

import Colors from '../../constants/colors'
import FiltersScreen from '../../screens/FiltersScreen'

const FiltersStack = createStackNavigator({
  Filters: FiltersScreen
}, {
  defaultNavigationOptions: {
      headerStyle: Platform.select({
        android: { backgroundColor: Colors.primaryColor },

      }),
      headerTintColor: Platform.select({
        android: "#FFF",
        ios: Colors.primaryColor
        
      })
  }
})

export default FiltersStack;