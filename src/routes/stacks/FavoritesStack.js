import { Platform } from 'react-native'
import { createStackNavigator } from 'react-navigation-stack'

import FavoritesScreen from '../../screens/FavoritesScreen'
import MealDetailsScreen from '../../screens/MealDetailsScreen'

import Colors from '../../constants/colors'

const Favoritesstack = createStackNavigator({
  
  Favorites: {
    screen: FavoritesScreen,

  },
  MealDetails: {
    screen: MealDetailsScreen,

  }
}, {
  defaultNavigationOptions: {
    headerTitle: "Favorite Meals",
      headerStyle: Platform.select({
        android: { backgroundColor: Colors.primaryColor },

      }),
      headerTintColor: Platform.select({
        android: "#FFF",
        ios: Colors.primaryColor
        
      })
  }
})

export default Favoritesstack;
