import { createDrawerNavigator } from 'react-navigation-drawer'

import TabsNavigator from '../tabs/TabsNavigator'
import FilterStack from '../stacks/FiltersStack'
import Colors from '../../constants/colors';

const DrawerNavigator = createDrawerNavigator({
  Meals: TabsNavigator,
  Filters: FilterStack
}, {
  contentOptions: {
    activeTintColor: Colors.accentColor
  }
})

export default DrawerNavigator;