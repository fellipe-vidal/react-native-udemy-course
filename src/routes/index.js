import { createAppContainer } from 'react-navigation'
import DrawerNavigator from './drawer/DrawerNavigator'

const Navigator = createAppContainer(DrawerNavigator)

export default Navigator;