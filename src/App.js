import React from 'react'
import { enableScreens } from 'react-native-screens'
import { createStore, combineReducers } from 'redux'
import { Provider } from 'react-redux'

import Navigator from './routes/index'
import mealsReducer from './store/reducers/meals'

enableScreens()

const rootReducer = combineReducers({meals: mealsReducer})
const store = createStore(rootReducer);

const App = () => {
  return (
    <Provider store={store}>
      <Navigator />
    </Provider>
  )
}

export default App
